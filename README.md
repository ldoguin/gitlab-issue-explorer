# Gitlab Issues Graph

This project allows you to navigate through linked issues in your Gitlab Project.

![Gitlab issue Graph example](/graphshot.png "Issues Graph")

## Installation

Run `npm install`.

## How to

To query gitlab, you need to setup a personal API token and a Gitlab URL.

```
export GITLAB_TOKEN=XXXXXXXXx
export GITLAB_URL=https://yourgitlab.com/api/v4/ # default to Gitlab.com
```

Run `npm start`

Go to `localhost:8080` and start browsing your project, there is currently no waiter, be patient :)