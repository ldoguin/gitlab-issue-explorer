'use strict'
const axios = require('axios')
const process = require('process')
const Bell = require('@hapi/bell');
const Inert = require('@hapi/inert');
const Hapi = require('@hapi/hapi')
const Boom = require('@hapi/boom')
const Cookie = require('@hapi/cookie')
const HapiXFwd = require('hapi-forwarded-for');
const Path = require('path');
const internals = {};

const gitlabURI = process.env.GITLAB_URL || 'https://gitlab.com';
const gitlabAPIURI = gitlabURI + '/api/v4/';

internals.start = async function () {
    const server = Hapi.server({
      host: '0.0.0.0',
      port: 8080,
      routes: {
          files: {
              relativeTo: Path.join(__dirname, 'public')
          },
          auth: {
            mode: 'required',
            strategy: 'session'
          },
      }
    });
    await server.register(HapiXFwd); // auto redirect to https based on proxy headers
    await server.register(Bell); // authenticaton
    await server.register(Cookie); // session management
    server.auth.strategy('gitlab', 'bell', {
        provider: 'gitlab',
        config: {uri: gitlabURI},
        password: process.env.GITLAB_COOKIE_ENCRYPTION_PASSWORD || 'cookie_encryption_password_secure_gitlab',
        isSecure: true,
        clientId: process.env.GITLAB_CLIENT_ID || 'CHANGEME',
        clientSecret: process.env.GITLAB_CLIENT_SECRET || 'CHANGEME',
        location: process.env.SITE_LOCATION || "https://yourwebsite.com"
    });
    server.auth.strategy('session', 'cookie', {
      cookie: {
          name: 'graphlabi-explorer',
          password: process.env.SESSION_COOKIE_ENCRYPTION_PASSWORD || 'password-should-be-super-32secrettsacters',
          isSecure: true
      },
      redirectTo: '/login',
  });

  server.route({
    method: ['GET', 'POST'],    // Must handle both GET and POST
    path: '/login',             // The callback endpoint registered with the provider
    options: {
        auth: {
          mode: 'required',
          strategy: 'gitlab'
        }
      },
      handler: async (request, h) => {
        if (!request.auth.isAuthenticated) {
          throw Boom.unauthorized('Authentication failed: ' + request.auth.error.message);
        }
        request.cookieAuth.set(request.auth.credentials);
        return h.redirect('/');
      }
  });

  server.route({
    method: 'GET',
    path: '/logout',
    options: {
        handler: async (request, h) => {
            request.cookieAuth.clear();
            return h.redirect('/');
        }
    }
  });

  await server.register(Inert);
  server.route({
      method: 'GET',
      path: '/{param*}',
      handler: {
          directory: {
              path: '.',
              redirectToSlash: true
          }
      }
  });

  server.route({
    method: 'GET',
    path: '/api/groups',
    handler: async (request, h) => {
      let elements = await getallElements("groups", request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/groups/{id}/projects',
    handler: async (request, h) => {
      let elements = await getallElements(`groups/${request.params.id}/projects`, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/projects/{id}/issues',
    handler: async (request, h) => {
      let elements = await getallElements(`projects/${request.params.id}/issues`, request.auth.credentials.token);
      await linkIssues(elements, request.auth.credentials.token);
      await deepLink(elements, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/projects/{id}/issues/{iid}',
    handler: async (request, h) => {
      let issue = await getOne(`projects/${request.params.id}/issues/${request.params.iid}`, request.auth.credentials.token);
      let links = await linkIssue(issue, request.auth.credentials.token);
      issue.mylinks = links;
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/projects/{id}/issues/{iid}/links',
    handler: async (request, h) => {
      let elements = await getOne(`projects/${request.params.id}/issues/${request.params.iid}`, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/groups/{id}/milestones',
    handler: async (request, h) => {
      let elements = await getallElements(`/groups/${request.params.id}/milestones`, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/groups/{id}/milestones/{mid}/issues',
    handler: async (request, h) => {
      let elements = await getallElements(`/groups/${request.params.id}/milestones/${request.params.mid}/issues`, request.auth.credentials.token);
      await linkIssues(elements, request.auth.credentials.token);
      await deepLink(elements, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/projects/{id}/milestones',
    handler: async (request, h) => {
      let elements = await getallElements(`/projects/${request.params.id}/milestones`, request.auth.credentials.token);
      return elements;
    }
  });

  server.route({
    method: 'GET',
    path: '/api/projects/{id}/milestones/{mid}/issues',
    handler: async (request, h) => {
      let elements = await getallElements(`/projects/${request.params.id}/milestones/${request.params.mid}/issues`, request.auth.credentials.token);
      await linkIssues(elements, request.auth.credentials.token);
      await deepLink(elements, request.auth.credentials.token);
      return elements;
    }
  });

  await server.start();
  console.log('Server started at:', server.info.uri);
};

internals.start();

const instance = axios.create({
  baseURL: gitlabAPIURI,
  timeout: 10000,
});

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

function parseLinksHeaders(data) {
  let arrData = data.split(",");
  let parsed_data = {};
  for (let d in arrData){
      let linkInfo = /<([^>]+)>;\s+rel="([^"]+)"/ig.exec(arrData[d]);
      parsed_data[linkInfo[2]]=linkInfo[1];
  }

  return parsed_data;
}

async function deepLink(elements, token) {
  let newLink = true;
  while (newLink) {
      await asyncForEach(Object.values(elements), async function (item,index) {
        if (item.mylinks) {
            await asyncForEach(item.mylinks, async function (itemLinked, index) {
            if (!elements[itemLinked.id]) {
              newLink = true;
              elements[itemLinked.id] = await getOne(`projects/${itemLinked.project_id}/issues/${itemLinked.iid}`, token);
              console.log(elements[itemLinked.id]);
            }
          });
        }
    });
    newLink = false;
  };
}

async function getallElements(urlpath, token) {
  let elements = {};
  let lastPage = false;
  let pageURL = urlpath + "?scope=all&per_page=100";
  while (!lastPage) {
    let els = await instance.get(pageURL,{headers: { Authorization: `Bearer ${token}` }});
    let rels = parseLinksHeaders(els.headers.link);
    if (!rels.next) {
      lastPage = true;
    } else {
      pageURL = rels.next;
    }
    els.data.forEach(function (item, index) {
      elements[item.id] = item;
    });
  }
  return elements;
}

async function getOne(urlpath, token) {
  let els = await instance.get(urlpath,{headers: { Authorization: `Bearer ${token}` }});
  return els.data;
}

  async function linkIssues(allIssues, token) {
    for (let issueIdx in allIssues){
      let issue = allIssues[issueIdx];
      let issueLinks = await linkIssue(issue, token);
      allIssues[issueIdx].mylinks = issueLinks;
    }
  }

  async function linkIssue(issue, token) {
    let pageURL = `/projects/${issue.project_id}/issues/${issue.iid}/links?per_page=100`;
    let linked = await instance.get(pageURL,{headers: { Authorization: `Bearer ${token}` }});
    let issueLinks = [];
    linked.data.forEach(function (item, index) {
      let alink = {
        link_type : item.link_type,
        id : item.id,
        iid: item.iid,
        project_id : item.project_id,
        state : item.state
      };
      issueLinks.push(alink);
    });
    return issueLinks;
  }